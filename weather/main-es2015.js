(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-container\">\n\n\n  <app-widget></app-widget>\n\n  <div>\n    <h1>Widget page</h1>\n    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut commodo nisl. Pellentesque ac erat urna.\n      Suspendisse ut\n      tempor ipsum, quis sollicitudin turpis. Curabitur ac dapibus nisi. In eget faucibus dui, et aliquam ligula. Nullam\n      consectetur turpis lacus, eu eleifend ex venenatis pharetra. Integer luctus sollicitudin magna sit amet semper.\n      Nunc\n      varius, est id tincidunt egestas, enim justo facilisis nisl, nec finibus arcu quam ac ante.</p>\n    <div _ngcontent-ami-c0=\"\" class=\"copyright\">© Copyright <a _ngcontent-ami-c0=\"\"\n        href=\"https://www.linkedin.com/in/dominik-szyja-b17111153/\" target=\"_blank\">Dominik Szyja</a></div>\n  </div>\n\n\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-container\">\n\n\n  <app-widget></app-widget>\n\n  <div>\n    <h1 >Widget page</h1>\n    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut commodo nisl. Pellentesque ac erat urna.\n      Suspendisse ut\n      tempor ipsum, quis sollicitudin turpis. Curabitur ac dapibus nisi. In eget faucibus dui, et aliquam ligula. Nullam\n      consectetur turpis lacus, eu eleifend ex venenatis pharetra. Integer luctus sollicitudin magna sit amet semper.\n      Nunc\n      varius, est id tincidunt egestas, enim justo facilisis nisl, nec finibus arcu quam ac ante.</p>\n    <div _ngcontent-ami-c0=\"\" class=\"copyright\">© Copyright <a _ngcontent-ami-c0=\"\"\n        href=\"https://www.linkedin.com/in/dominik-szyja-b17111153/\" target=\"_blank\">Dominik Szyja</a></div>\n  </div>\n\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/widget/widget.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/widget/widget.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"widget\" *ngIf=\"!isLoading\">\n  <div class=\"heading\">\n    <p class=\"area-name\">\n      <select (change)=\"makeRequest($event.target.value)\" style=\"border: 0;\">\n        <option *ngFor=\"let city of citiesData; trackBy:trackCityByFn\" [value]=\"city.id\"\n          [selected]=\"city.id === currentCityId\">{{ city.name }}\n        </option>\n      </select> </p>\n\n    <ng-container *ngIf=\"currentDay\">\n      <p class=\"area-info\">\n        {{ currentDay.date | date: 'EEEE MMM dd' }}th\n        <br />{{ currentDay.type }}\n      </p>\n      <div class=\"current-weather\">\n        <img src=\"../../assets/img/cloudy.png\" alt=\"Cloudy weather\">\n        <span class=\"value\">\n          {{ currentDay.temperature }}\n          <span class=\"degree-type\">&deg;C</span>\n        </span>\n\n        <div class=\"more\">\n          <div>\n            <span>Precipitation:</span>{{ currentDay.precipitation }}%\n          </div>\n          <div><span>Humidity:</span>{{currentDay.humidity}}%</div>\n          <div><span>Wind:</span>{{ currentDay.windInfo.speed }} mph {{ currentDay.windInfo.direction }}</div>\n          <div><span>Pollen Count:</span>{{ currentDay.pollenCount }}</div>\n        </div>\n\n      </div>\n    </ng-container>\n\n  </div>\n  <div class=\"days\">\n    <div class=\"day\" *ngFor=\"let day of weatherData; trackBy:trackByFn\">\n      <div class=\"title\">\n        {{ day.date | date: 'EEEE' }}\n      </div>\n      <div>\n        <img [src]=\"imageURL + getWeatherImageName(day.type)\" alt=\"\">\n      </div>\n      <div style=\"font-size: 1.2em; font-weight: 600;\">\n        {{ day.temperature }}&deg;\n      </div>\n      <div class=\"smaller\">\n        <span>Pollen</span> {{ day.pollenCount }}\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");




const routes = [
    { path: 'weather', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AppComponent = class AppComponent {
    constructor(router) {
        this.router = router;
        this.title = 'weather-app';
    }
    ngOnInit() {
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _widget_widget_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./widget/widget.component */ "./src/app/widget/widget.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");








let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _widget_widget_component__WEBPACK_IMPORTED_MODULE_5__["WidgetComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() { }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/weather.service.ts":
/*!************************************!*\
  !*** ./src/app/weather.service.ts ***!
  \************************************/
/*! exports provided: baseUrl, WEATHER_TYPE, WeatherService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WEATHER_TYPE", function() { return WEATHER_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WeatherService", function() { return WeatherService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



const baseUrl = 'https://dev-weather-api.azurewebsites.net';
const WEATHER_TYPE = {
    RainAndCloudy: 'rain_s_cloudy.png',
    RainLight: 'rain_light.png',
    Cloudy: 'cloudy.png',
    PartlyCloudy: 'partly_cloudy.png',
    Sunny: 'sunny.png'
};
let WeatherService = class WeatherService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = '/api/city';
        this.url = baseUrl + this.url;
    }
    getCitiesForDropdown() {
        return this.httpClient.get(this.url);
    }
    getWeather(cityId, date = null) {
        if (cityId) {
            if (!date) {
                date = new Date().toJSON();
            }
            return this.httpClient.get(`${this.url}/${cityId}/weather?date=${date}`);
        }
    }
};
WeatherService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
WeatherService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], WeatherService);



/***/ }),

/***/ "./src/app/widget/widget.component.scss":
/*!**********************************************!*\
  !*** ./src/app/widget/widget.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".widget {\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n  width: 100%;\n  max-width: 575px;\n}\n.widget .heading {\n  padding: 10px 6px;\n}\n.widget .heading p.area-name {\n  font-size: 2.5em;\n  font-weight: 400;\n  line-height: 1.2;\n  letter-spacing: 0.015em;\n}\n.widget .heading p.area-info {\n  font-size: 0.875em;\n  line-height: 1.3;\n  letter-spacing: -0.02em;\n}\n.widget .heading .current-weather {\n  min-height: 72px;\n  max-width: 415px;\n}\n.widget .heading .current-weather img {\n  float: left;\n}\n.widget .heading .current-weather .value {\n  font-size: 2em;\n  display: inline-block;\n  padding-top: 4px;\n  min-height: 48px;\n  padding-left: 2px;\n  color: #555355;\n}\n.widget .heading .current-weather .degree-type {\n  font-size: 0.8125em;\n  vertical-align: top;\n  float: right;\n}\n.widget .heading .current-weather .more {\n  line-height: 1.2;\n  font-size: 15px;\n  padding-right: 40px;\n  float: right;\n}\n.widget .heading .current-weather .more span {\n  padding-right: 4px;\n}\n.widget .days {\n  padding: 10px 21px;\n  display: flex;\n  flex-flow: row nowrap;\n  align-items: center;\n  justify-content: space-between;\n}\n.widget .days .day {\n  text-align: center;\n}\n.widget .days .day .title {\n  font-size: 13px;\n  font-weight: 600;\n}\n.widget .days .day .smaller {\n  font-size: 0.85rem;\n  color: #716f71;\n}\n.widget .area-info,\n.widget span {\n  color: #716f71;\n}\n.widget p {\n  margin: 0;\n  color: #353435;\n  line-height: 1.15;\n}\n@media all and (max-width: 515px) {\n  .widget .days {\n    width: 100%;\n    padding: 10px 0;\n    flex-direction: column;\n  }\n  .widget .days .day {\n    width: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    border-top: 1px solid #d7d6d7;\n  }\n  .widget .days .day .title {\n    text-align: left;\n    padding-left: 6px;\n  }\n  .widget .days .day > div {\n    max-width: 25%;\n    flex-basis: 25%;\n    width: 100%;\n  }\n}\n@media all and (max-width: 350px) {\n  .widget .heading .current-weather .more {\n    padding-right: 0;\n    transition: 0.6s all;\n  }\n  .widget select {\n    width: 100%;\n  }\n  .widget .days .day {\n    padding: 5px 0;\n  }\n  .widget .days .day img {\n    width: 35px;\n    height: 35px;\n  }\n}\n@media all and (max-width: 250px) {\n  .widget .heading {\n    font-size: 13px;\n  }\n  .widget .heading img {\n    width: 35px;\n    height: 35px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2lkZ2V0L0M6XFxVc2Vyc1xcMTgwY1xcRG9jdW1lbnRzXFx3ZWF0aGVyLWFwcC9zcmNcXGFwcFxcd2lkZ2V0XFx3aWRnZXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3dpZGdldC93aWRnZXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx3RUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0NGO0FEQ0U7RUFDRSxpQkFBQTtBQ0NKO0FEQ007RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ0NSO0FEQ007RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUNDUjtBREVJO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBQ0FOO0FEQ007RUFDRSxXQUFBO0FDQ1I7QURDTTtFQUNFLGNBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUNDUjtBRENNO0VBQ0UsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNDUjtBRENNO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDQ1I7QURBUTtFQUNFLGtCQUFBO0FDRVY7QURJRTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ0ZKO0FER0k7RUFLRSxrQkFBQTtBQ0xOO0FEQ007RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUNDUjtBREVNO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0FDQVI7QURLRTs7RUFFRSxjQUFBO0FDSEo7QURLRTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNISjtBRE9BO0VBRUk7SUFDRSxXQUFBO0lBRUEsZUFBQTtJQUNBLHNCQUFBO0VDTko7RURPSTtJQUNFLFdBQUE7SUFDQSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSw4QkFBQTtJQUNBLDZCQUFBO0VDTE47RURNTTtJQUNFLGdCQUFBO0lBQ0EsaUJBQUE7RUNKUjtFRE1NO0lBQ0UsY0FBQTtJQUNBLGVBQUE7SUFDQSxXQUFBO0VDSlI7QUFDRjtBRFVBO0VBRUk7SUFDRSxnQkFBQTtJQUNBLG9CQUFBO0VDVEo7RURXRTtJQUNFLFdBQUE7RUNUSjtFRFlJO0lBQ0UsY0FBQTtFQ1ZOO0VEV007SUFDRSxXQUFBO0lBQ0EsWUFBQTtFQ1RSO0FBQ0Y7QURlQTtFQUNFO0lBQ0UsZUFBQTtFQ2JGO0VEY0U7SUFDRSxXQUFBO0lBQ0EsWUFBQTtFQ1pKO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC93aWRnZXQvd2lkZ2V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndpZGdldCB7XHJcbiAgYm94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjQpO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1heC13aWR0aDogNTc1cHg7XHJcblxyXG4gIC5oZWFkaW5nIHtcclxuICAgIHBhZGRpbmc6IDEwcHggNnB4O1xyXG4gICAgcCB7XHJcbiAgICAgICYuYXJlYS1uYW1lIHtcclxuICAgICAgICBmb250LXNpemU6IDIuNWVtO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMjtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4wMTVlbTtcclxuICAgICAgfVxyXG4gICAgICAmLmFyZWEtaW5mbyB7XHJcbiAgICAgICAgZm9udC1zaXplOiAwLjg3NWVtO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjM7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC0wLjAyZW07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC5jdXJyZW50LXdlYXRoZXIge1xyXG4gICAgICBtaW4taGVpZ2h0OiA3MnB4O1xyXG4gICAgICBtYXgtd2lkdGg6IDQxNXB4O1xyXG4gICAgICBpbWcge1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICB9XHJcbiAgICAgIC52YWx1ZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyZW07XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA0cHg7XHJcbiAgICAgICAgbWluLWhlaWdodDogNDhweDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDJweDtcclxuICAgICAgICBjb2xvcjogIzU1NTM1NTtcclxuICAgICAgfVxyXG4gICAgICAuZGVncmVlLXR5cGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMC44MTI1ZW07XHJcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgIH1cclxuICAgICAgLm1vcmUge1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDQwcHg7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgcGFkZGluZy1yaWdodDogNHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLmRheXMge1xyXG4gICAgcGFkZGluZzogMTBweCAyMXB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93IG5vd3JhcDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAuZGF5IHtcclxuICAgICAgLnRpdGxlIHtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgfVxyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIC5zbWFsbGVyIHtcclxuICAgICAgICBmb250LXNpemU6IDAuODVyZW07XHJcbiAgICAgICAgY29sb3I6ICM3MTZmNzE7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5hcmVhLWluZm8sXHJcbiAgc3BhbiB7XHJcbiAgICBjb2xvcjogIzcxNmY3MTtcclxuICB9XHJcbiAgcCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBjb2xvcjogIzM1MzQzNTtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjE1O1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNTE1cHgpIHtcclxuICAud2lkZ2V0IHtcclxuICAgIC5kYXlzIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgICBwYWRkaW5nOiAxMHB4IDA7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgIC5kYXkge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkN2Q2ZDc7XHJcbiAgICAgICAgLnRpdGxlIHtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDZweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgJiA+IGRpdiB7XHJcbiAgICAgICAgICBtYXgtd2lkdGg6IDI1JTtcclxuICAgICAgICAgIGZsZXgtYmFzaXM6IDI1JTtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogMzUwcHgpIHtcclxuICAud2lkZ2V0IHtcclxuICAgIC5oZWFkaW5nIC5jdXJyZW50LXdlYXRoZXIgLm1vcmUge1xyXG4gICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gICAgICB0cmFuc2l0aW9uOiAuNnMgYWxsO1xyXG4gICAgfVxyXG4gICAgc2VsZWN0IHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuZGF5cyB7XHJcbiAgICAgIC5kYXkge1xyXG4gICAgICAgIHBhZGRpbmc6IDVweCAwO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICB3aWR0aDogMzVweDtcclxuICAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDI1MHB4KSB7XHJcbiAgLndpZGdldCAuaGVhZGluZyB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMzVweDtcclxuICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iLCIud2lkZ2V0IHtcbiAgYm94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjQpO1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiA1NzVweDtcbn1cbi53aWRnZXQgLmhlYWRpbmcge1xuICBwYWRkaW5nOiAxMHB4IDZweDtcbn1cbi53aWRnZXQgLmhlYWRpbmcgcC5hcmVhLW5hbWUge1xuICBmb250LXNpemU6IDIuNWVtO1xuICBmb250LXdlaWdodDogNDAwO1xuICBsaW5lLWhlaWdodDogMS4yO1xuICBsZXR0ZXItc3BhY2luZzogMC4wMTVlbTtcbn1cbi53aWRnZXQgLmhlYWRpbmcgcC5hcmVhLWluZm8ge1xuICBmb250LXNpemU6IDAuODc1ZW07XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG4gIGxldHRlci1zcGFjaW5nOiAtMC4wMmVtO1xufVxuLndpZGdldCAuaGVhZGluZyAuY3VycmVudC13ZWF0aGVyIHtcbiAgbWluLWhlaWdodDogNzJweDtcbiAgbWF4LXdpZHRoOiA0MTVweDtcbn1cbi53aWRnZXQgLmhlYWRpbmcgLmN1cnJlbnQtd2VhdGhlciBpbWcge1xuICBmbG9hdDogbGVmdDtcbn1cbi53aWRnZXQgLmhlYWRpbmcgLmN1cnJlbnQtd2VhdGhlciAudmFsdWUge1xuICBmb250LXNpemU6IDJlbTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nLXRvcDogNHB4O1xuICBtaW4taGVpZ2h0OiA0OHB4O1xuICBwYWRkaW5nLWxlZnQ6IDJweDtcbiAgY29sb3I6ICM1NTUzNTU7XG59XG4ud2lkZ2V0IC5oZWFkaW5nIC5jdXJyZW50LXdlYXRoZXIgLmRlZ3JlZS10eXBlIHtcbiAgZm9udC1zaXplOiAwLjgxMjVlbTtcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuLndpZGdldCAuaGVhZGluZyAuY3VycmVudC13ZWF0aGVyIC5tb3JlIHtcbiAgbGluZS1oZWlnaHQ6IDEuMjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xuICBmbG9hdDogcmlnaHQ7XG59XG4ud2lkZ2V0IC5oZWFkaW5nIC5jdXJyZW50LXdlYXRoZXIgLm1vcmUgc3BhbiB7XG4gIHBhZGRpbmctcmlnaHQ6IDRweDtcbn1cbi53aWRnZXQgLmRheXMge1xuICBwYWRkaW5nOiAxMHB4IDIxcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93IG5vd3JhcDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLndpZGdldCAuZGF5cyAuZGF5IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLndpZGdldCAuZGF5cyAuZGF5IC50aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi53aWRnZXQgLmRheXMgLmRheSAuc21hbGxlciB7XG4gIGZvbnQtc2l6ZTogMC44NXJlbTtcbiAgY29sb3I6ICM3MTZmNzE7XG59XG4ud2lkZ2V0IC5hcmVhLWluZm8sXG4ud2lkZ2V0IHNwYW4ge1xuICBjb2xvcjogIzcxNmY3MTtcbn1cbi53aWRnZXQgcCB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICMzNTM0MzU7XG4gIGxpbmUtaGVpZ2h0OiAxLjE1O1xufVxuXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA1MTVweCkge1xuICAud2lkZ2V0IC5kYXlzIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuICAud2lkZ2V0IC5kYXlzIC5kYXkge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNkN2Q2ZDc7XG4gIH1cbiAgLndpZGdldCAuZGF5cyAuZGF5IC50aXRsZSB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBwYWRkaW5nLWxlZnQ6IDZweDtcbiAgfVxuICAud2lkZ2V0IC5kYXlzIC5kYXkgPiBkaXYge1xuICAgIG1heC13aWR0aDogMjUlO1xuICAgIGZsZXgtYmFzaXM6IDI1JTtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogMzUwcHgpIHtcbiAgLndpZGdldCAuaGVhZGluZyAuY3VycmVudC13ZWF0aGVyIC5tb3JlIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgIHRyYW5zaXRpb246IDAuNnMgYWxsO1xuICB9XG4gIC53aWRnZXQgc2VsZWN0IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuICAud2lkZ2V0IC5kYXlzIC5kYXkge1xuICAgIHBhZGRpbmc6IDVweCAwO1xuICB9XG4gIC53aWRnZXQgLmRheXMgLmRheSBpbWcge1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgfVxufVxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogMjUwcHgpIHtcbiAgLndpZGdldCAuaGVhZGluZyB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9XG4gIC53aWRnZXQgLmhlYWRpbmcgaW1nIHtcbiAgICB3aWR0aDogMzVweDtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/widget/widget.component.ts":
/*!********************************************!*\
  !*** ./src/app/widget/widget.component.ts ***!
  \********************************************/
/*! exports provided: WidgetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetComponent", function() { return WidgetComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _weather_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../weather.service */ "./src/app/weather.service.ts");



let WidgetComponent = class WidgetComponent {
    constructor(weatherService) {
        this.weatherService = weatherService;
        this.imageURL = 'assets/img/';
        this.isLoading = true;
    }
    getWeatherImageName(type) {
        return (type in _weather_service__WEBPACK_IMPORTED_MODULE_2__["WEATHER_TYPE"] && _weather_service__WEBPACK_IMPORTED_MODULE_2__["WEATHER_TYPE"][type]) ? _weather_service__WEBPACK_IMPORTED_MODULE_2__["WEATHER_TYPE"][type] : 'blank.png';
    }
    trackByFn(index, item) {
        return item ? item.date : null;
    }
    trackCityByFn(index, item) {
        return item ? item.date : null;
    }
    makeRequest(cityId) {
        if (cityId) {
            this.weatherService.getWeather(cityId).subscribe(response => {
                if (response) {
                    this.weatherData = response;
                    this.currentDay = this.weatherData.shift();
                }
            }, err => console.log(err.error), () => {
                this.isLoading = false;
            });
        }
    }
    ngOnInit() {
        this.weatherService.getCitiesForDropdown().subscribe(response => {
            if (response && response.length > 0) {
                this.citiesData = response;
                this.currentCityId = response[0].id;
                this.makeRequest(this.currentCityId);
            }
        });
    }
};
WidgetComponent.ctorParameters = () => [
    { type: _weather_service__WEBPACK_IMPORTED_MODULE_2__["WeatherService"] }
];
WidgetComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-widget',
        template: __webpack_require__(/*! raw-loader!./widget.component.html */ "./node_modules/raw-loader/index.js!./src/app/widget/widget.component.html"),
        styles: [__webpack_require__(/*! ./widget.component.scss */ "./src/app/widget/widget.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_weather_service__WEBPACK_IMPORTED_MODULE_2__["WeatherService"]])
], WidgetComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\180c\Documents\weather-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map