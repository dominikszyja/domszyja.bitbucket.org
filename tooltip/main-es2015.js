(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"parent-container\">\n\n    <h1 class=\"t-center\" style=\"margin: 2em 0 1em;\" appTooltip=\"Wszystkie pogrubione elementy posiadają tooltipy, niektóre z nich można zobaczyć 'na kliknięcie' <strong>test</strong>\">\n        Tooltip </h1>\n    <p>\n        Lorem ipsum dolor <span appTooltip=\"Tooltip\">sit amet</span>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Feugiat pretium nibh ipsum consequat nisl vel pretium. Euismod nisi porta lorem mollis.\n        Nunc sed augue lacus viverra vitae congue. Id diam vel quam elementum pulvinar. Habitant morbi tristique senectus et netus et malesuada fames. Neque egestas congue quisque egestas diam in arcu cursus. Sollicitudin ac orci phasellus egestas tellus\n        rutrum tellus pellentesque eu. Mi ipsum faucibus vitae aliquet nec ullamcorper sit amet risus. Enim tortor at auctor urna nunc id cursus. Cursus risus at ultrices mi tempus imperdiet nulla malesuada.\n    </p>\n    <p>\n        Lobortis feugiat vivamus at augue eget. Dignissim sodales ut eu sem integer. <span appTooltip=\"Inny tooltip\">Semper risus in hendrerit gravida.</span> Suspendisse potenti nullam ac tortor vitae. Lacus suspendisse faucibus interdum posuere lorem\n        ipsum dolor sit. Euismod lacinia at quis risus. Molestie a iaculis at erat. Consequat interdum varius sit amet mattis vulputate. Cras tincidunt lobortis feugiat vivamus at augue. Convallis convallis tellus id interdum. Blandit volutpat maecenas\n        volutpat blandit aliquam etiam erat velit scelerisque. Augue neque gravida in fermentum et sollicitudin ac orci phasellus.\n    </p>\n    <p>\n        Cursus risus at ultrices mi tempus imperdiet nulla malesuada. <span appTooltip=\"Na kliknięcie\" appTooltipType='click'>Justo eget magna\n            fermentum</span> iaculis eu non diam phasellus. A diam sollicitudin tempor id eu nisl nunc mi. Dignissim sodales ut eu sem integer vitae justo eget magna. Scelerisque felis imperdiet proin fermentum. Enim eu turpis egestas pretium. Ac tortor\n        vitae purus faucibus ornare suspendisse sed nisi lacus. Ipsum dolor sit amet consectetur adipiscing elit ut. Dolor sit amet consectetur adipiscing elit duis tristique. Tortor at auctor urna nunc. Mi eget mauris pharetra et ultrices neque. Augue\n        lacus viverra vitae congue eu consequat. Auctor augue mauris augue neque gravida. Lobortis mattis aliquam faucibus purus. Tellus cras adipiscing enim eu turpis egestas pretium. Sit amet aliquam id diam maecenas ultricies mi eget mauris. Diam volutpat\n        commodo sed egestas egestas fringilla. Convallis posuere morbi leo urna.\n    </p>\n\n    <p>\n        <span appTooltip=\"Na kliknięcie cała sekcja\" appTooltipType='click'>Pellentesque eu tincidunt tortor aliquam nulla. Ac tortor dignissim convallis aenean et.</span> Ipsum dolor sit amet consectetur adipiscing. Montes nascetur ridiculus mus mauris\n        vitae. Aenean euismod elementum nisi quis eleifend quam adipiscing vitae. Hac habitasse platea dictumst quisque sagittis purus sit. Velit aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Auctor augue mauris augue neque. Ipsum\n        suspendisse ultrices gravida dictum fusce ut placerat. Diam ut venenatis tellus in metus vulputate eu scelerisque felis. Elit eget gravida cum sociis natoque. Morbi non arcu risus quis varius quam. Cras sed felis eget velit aliquet sagittis id.\n        Cursus turpis massa tincidunt dui ut ornare lectus. Massa massa ultricies mi quis hendrerit dolor. Lorem ipsum dolor sit amet consectetur adipiscing. Arcu non sodales neque sodales ut etiam sit\n    </p>\n\n    <div class=\"copyright\">&#169; Copyright <a href=\"https://www.linkedin.com/in/dominik-szyja-b17111153/\" target=\"_blank\">Dominik\n            Szyja</a>\n    </div>\n</div>\n\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/tooltip/tooltip.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tooltip/tooltip.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"tooltip\" [ngClass]=\"{'active': isVisible}\" [innerHTML]=\"content\">\r\n    <!-- {{ content }} -->\r\n</div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".t-center {\n  text-align: center;\n}\n\n.test {\n  position: fixed;\n}\n\n.test .tooltip {\n  position: absolute;\n  top: 0;\n  display: block;\n  background: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXFVzZXJzXFxEb21pbmlrXFxzYXJlLWV4Y2VyY2lzZS9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQ0NKOztBREdBO0VBQ0ksZUFBQTtBQ0FKOztBRENJO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNDUiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50LWNlbnRlciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4udGVzdCB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAudG9vbHRpcCB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMDtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgICB9XHJcbn0iLCIudC1jZW50ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXN0IHtcbiAgcG9zaXRpb246IGZpeGVkO1xufVxuLnRlc3QgLnRvb2x0aXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6IHJlZDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'sare-excercise';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _tooltip_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tooltip.directive */ "./src/app/tooltip.directive.ts");
/* harmony import */ var _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tooltip/tooltip.component */ "./src/app/tooltip/tooltip.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _cache_interceptor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./cache-interceptor.service */ "./src/app/cache-interceptor.service.ts");









let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _tooltip_directive__WEBPACK_IMPORTED_MODULE_5__["TooltipDirective"],
            _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_6__["TooltipComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]
        ],
        providers: [
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HTTP_INTERCEPTORS"], useClass: _cache_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["CacheInterceptor"], multi: true }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
        entryComponents: [_tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_6__["TooltipComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/cache-interceptor.service.ts":
/*!**********************************************!*\
  !*** ./src/app/cache-interceptor.service.ts ***!
  \**********************************************/
/*! exports provided: CacheInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CacheInterceptor", function() { return CacheInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CacheInterceptor = class CacheInterceptor {
    intercept(req, next) {
        const nextReq = req.clone({
            headers: req.headers.set('Cache-Control', 'no-cache')
                .set('Pragma', 'no-cache')
                .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT')
                .set('If-Modified-Since', '0')
        });
        return next.handle(nextReq);
    }
};
CacheInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], CacheInterceptor);



/***/ }),

/***/ "./src/app/tooltip.directive.ts":
/*!**************************************!*\
  !*** ./src/app/tooltip.directive.ts ***!
  \**************************************/
/*! exports provided: TooltipDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipDirective", function() { return TooltipDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tooltip/tooltip.component */ "./src/app/tooltip/tooltip.component.ts");
/* harmony import */ var _tooltip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tooltip.service */ "./src/app/tooltip.service.ts");




let TooltipDirective = class TooltipDirective {
    constructor(elementRef, viewContainerRef, componentFactoryResolver, renderer, tooltipService, _changeDetectionRef) {
        this.elementRef = elementRef;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.renderer = renderer;
        this.tooltipService = tooltipService;
        this._changeDetectionRef = _changeDetectionRef;
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.appTooltipType = this.appTooltipType || 'hover';
        if (!(this.appTooltipType === 'hover' || this.appTooltipType === 'click')) {
            this.appTooltipType = 'hover';
        }
        // this.appTooltipType = ['hover', 'click'].includes(this.appTooltipType) ? this.appTooltipType : 'hover';
        this.initTooltip();
    }
    initTooltip() {
        this.renderer.addClass(this.elementRef.nativeElement, 'tooltip-wrapper');
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(_tooltip_tooltip_component__WEBPACK_IMPORTED_MODULE_2__["TooltipComponent"]);
        this.viewContainerRef.clear();
        const componentRef = this.viewContainerRef.createComponent(componentFactory);
        componentRef.instance.content = this.appTooltip;
        componentRef.instance.type = this.appTooltipType;
        this.renderer.appendChild(this.elementRef.nativeElement, componentRef.location.nativeElement);
        const id = Math.random().toString(36).substr(2, 9);
        this.renderer.setAttribute(this.elementRef.nativeElement, '_tooltip', id);
        this.tooltipService.addTooltip(componentRef, id);
        this._changeDetectionRef.detectChanges();
    }
    onMouseEnter() {
        if (this.appTooltipType === 'hover') {
            this.toggleTooltip();
        }
    }
    onMouseLeave() {
        if (this.appTooltipType === 'hover') {
            this.toggleTooltip(false);
        }
    }
    onClick() {
        if (this.appTooltipType === 'click') {
            this.toggleTooltip();
            this.renderer.setAttribute(this.elementRef.nativeElement, '_touched', 'true');
        }
    }
    onOutsideClick(targetElement) {
        if (this.appTooltipType === 'click') {
            if (this.elementRef.nativeElement.getAttribute('_touched')) {
                const clickedInside = this.elementRef.nativeElement.contains(targetElement);
                if (!clickedInside) {
                    this.renderer.removeAttribute(this.elementRef.nativeElement, 'touched');
                    this.toggleTooltip(false);
                }
            }
        }
    }
    toggleTooltip(show = true) {
        const id = this.elementRef.nativeElement.getAttribute('_tooltip');
        if (id) {
            const _componentRef = this.tooltipService.getTooltipInstance(id);
            return !_componentRef.instance.isVisible && show ? _componentRef.instance.showComponent() : _componentRef.instance.hideComponent();
        }
    }
    ngOnDestroy() {
        const id = this.elementRef.nativeElement.getAttribute('_tooltip');
        this.tooltipService.removeTooltip(id);
    }
};
TooltipDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
    { type: _tooltip_service__WEBPACK_IMPORTED_MODULE_3__["TooltipService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], TooltipDirective.prototype, "appTooltip", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], TooltipDirective.prototype, "appTooltipType", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseenter')
], TooltipDirective.prototype, "onMouseEnter", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseleave')
], TooltipDirective.prototype, "onMouseLeave", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click')
], TooltipDirective.prototype, "onClick", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event.target'])
], TooltipDirective.prototype, "onOutsideClick", null);
TooltipDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appTooltip]'
    })
], TooltipDirective);



/***/ }),

/***/ "./src/app/tooltip.service.ts":
/*!************************************!*\
  !*** ./src/app/tooltip.service.ts ***!
  \************************************/
/*! exports provided: TooltipService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipService", function() { return TooltipService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TooltipService = class TooltipService {
    constructor() {
        this.tooltips = {};
    }
    getTooltipInstance(id) {
        if (id in this.tooltips && this.tooltips[id]) {
            return this.tooltips[id];
        }
        return false;
    }
    addTooltip(instance, id) {
        this.tooltips[id] = instance;
    }
    removeTooltip(id) {
        if (id in this.tooltips && this.tooltips[id]) {
            delete this.tooltips[id];
        }
    }
};
TooltipService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], TooltipService);



/***/ }),

/***/ "./src/app/tooltip/tooltip.component.scss":
/*!************************************************!*\
  !*** ./src/app/tooltip/tooltip.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tooltip-wrapper {\n  position: relative !important;\n  font-weight: 600;\n}\n.tooltip-wrapper app-tooltip .tooltip {\n  position: absolute;\n  left: 50%;\n  -webkit-transform: translateX(-50%) translateY(-100%);\n      -ms-transform: translateX(-50%) translateY(-100%);\n          transform: translateX(-50%) translateY(-100%);\n  top: -8px;\n  min-width: 80px;\n  padding: 7px 12px;\n  font-size: 0.8rem;\n  font-weight: 500;\n  background: #000;\n  color: #fff;\n  border-radius: 3px;\n  -webkit-transition: all 0.5s cubic-bezier(0, 0, 0.2, 1);\n  transition: all 0.5s cubic-bezier(0, 0, 0.2, 1);\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  cursor: default;\n  opacity: 0;\n  visibility: hidden;\n  text-align: center;\n  letter-spacing: -0.02em;\n  line-height: 1.3;\n}\n.tooltip-wrapper app-tooltip .tooltip.active {\n  opacity: 1;\n  visibility: visible;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdG9vbHRpcC9DOlxcVXNlcnNcXERvbWluaWtcXHNhcmUtZXhjZXJjaXNlL3NyY1xcYXBwXFx0b29sdGlwXFx0b29sdGlwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC90b29sdGlwL3Rvb2x0aXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw2QkFBQTtFQUNBLGdCQUFBO0FDQ0o7QURBSTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHFEQUFBO01BQUEsaURBQUE7VUFBQSw2Q0FBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx1REFBQTtFQUFBLCtDQUFBO0VBQ0EseUJBQUE7S0FBQSxzQkFBQTtNQUFBLHFCQUFBO1VBQUEsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FDRVI7QUREUTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtBQ0daIiwiZmlsZSI6InNyYy9hcHAvdG9vbHRpcC90b29sdGlwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvb2x0aXAtd3JhcHBlciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBhcHAtdG9vbHRpcCAudG9vbHRpcCB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtMTAwJSk7XHJcbiAgICAgICAgdG9wOiAtOHB4O1xyXG4gICAgICAgIG1pbi13aWR0aDogODBweDtcclxuICAgICAgICBwYWRkaW5nOiA3cHggMTJweDtcclxuICAgICAgICBmb250LXNpemU6IDAuOHJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLCAwLCAwLjIsIDEpO1xyXG4gICAgICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC0wLjAyZW07XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMztcclxuICAgICAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLnRvb2x0aXAtd3JhcHBlciB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLnRvb2x0aXAtd3JhcHBlciBhcHAtdG9vbHRpcCAudG9vbHRpcCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgdHJhbnNsYXRlWSgtMTAwJSk7XG4gIHRvcDogLThweDtcbiAgbWluLXdpZHRoOiA4MHB4O1xuICBwYWRkaW5nOiA3cHggMTJweDtcbiAgZm9udC1zaXplOiAwLjhyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGN1YmljLWJlemllcigwLCAwLCAwLjIsIDEpO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgY3Vyc29yOiBkZWZhdWx0O1xuICBvcGFjaXR5OiAwO1xuICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjAyZW07XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG59XG4udG9vbHRpcC13cmFwcGVyIGFwcC10b29sdGlwIC50b29sdGlwLmFjdGl2ZSB7XG4gIG9wYWNpdHk6IDE7XG4gIHZpc2liaWxpdHk6IHZpc2libGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/tooltip/tooltip.component.ts":
/*!**********************************************!*\
  !*** ./src/app/tooltip/tooltip.component.ts ***!
  \**********************************************/
/*! exports provided: TooltipComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TooltipComponent", function() { return TooltipComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let TooltipComponent = class TooltipComponent {
    constructor() {
    }
    ngOnInit() {
    }
    showComponent() {
        this.isVisible = true;
    }
    hideComponent() {
        this.isVisible = false;
    }
};
TooltipComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tooltip',
        template: __webpack_require__(/*! raw-loader!./tooltip.component.html */ "./node_modules/raw-loader/index.js!./src/app/tooltip/tooltip.component.html"),
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        styles: [__webpack_require__(/*! ./tooltip.component.scss */ "./src/app/tooltip/tooltip.component.scss")]
    })
], TooltipComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Dominik\sare-excercise\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map