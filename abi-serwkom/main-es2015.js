(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<div class=\"parent-container\">\n    <app-home></app-home>\n    <div _ngcontent-ami-c0=\"\" class=\"copyright\">© Copyright <a _ngcontent-ami-c0=\"\" href=\"https://www.linkedin.com/in/dominik-szyja-b17111153/\" target=\"_blank\">Dominik Szyja</a></div>\n</div>\n\n\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"column-half first\">\n        <p> <span class=\"text-rotation\">Welcome Back!</span></p>\n        <p *ngIf=\"isLoading\" class=\"loading-indicator\">Loading....</p>\n        <ul class=\"links\" [ngStyle]=\"{'min-height': pageSize ? pageSize * 20 + 'px' : auto }\">\n            <ng-container *ngIf=\"!isLoading\">\n                <li *ngFor=\"let image of imageData\" (click)=\"activeImage = image;\" [ngClass]=\"{'active': activeImage && activeImage.id === image.id}\">\n                    <a>{{ image.title }}</a>\n                </li>\n            </ng-container>\n\n        </ul>\n        <div class=\"pagination\" *ngIf=\"pagination\">\n            Page: <span class=\"active\">{{ pageIndex }}</span>\n            <span *ngFor=\"let page of pagination\" (click)=\"onPage(page.params)\">\n                {{ page.name }}\n            </span>\n        </div>\n    </div>\n    <div class=\"column-half second\">\n\n        <img *ngIf=\"activeImage\" class='active-image' [src]=\"activeImage.thumbnailUrl\" [alt]=\"activeImage.title\" [appImageHover]=\"activeImage\">\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");




const routes = [
    { path: 'abi-serwkom', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'abi-serwkom';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _image_hover_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./image-hover.directive */ "./src/app/image-hover.directive.ts");








let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            _image_hover_directive__WEBPACK_IMPORTED_MODULE_7__["ImageHoverDirective"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-flow: row wrap;\n}\n\n.column-half {\n  flex-basis: 50%;\n  max-width: 50%;\n  width: 100%;\n}\n\n.links span,\n.links li,\n.pagination span,\n.pagination li {\n  cursor: pointer;\n  font-weight: 300;\n  line-height: 20px;\n}\n\n.links span.active, .links span:hover,\n.links li.active,\n.links li:hover,\n.pagination span.active,\n.pagination span:hover,\n.pagination li.active,\n.pagination li:hover {\n  font-weight: 400;\n  color: #175BCE;\n  border-color: #175BCE;\n}\n\n.links span.active,\n.links li.active,\n.pagination span.active,\n.pagination li.active {\n  cursor: default;\n}\n\n.links {\n  padding-left: 1.2em;\n}\n\n.text-rotation {\n  -webkit-transform: rotate(180deg);\n          transform: rotate(180deg);\n  display: block;\n  text-align: center;\n}\n\n.loading-indicator {\n  position: absolute;\n}\n\n.pagination span {\n  display: inline-block;\n  margin: 0 5px;\n  padding: 9px 14px;\n  border: 1px solid #333;\n}\n\n.active-image {\n  -o-object-position: center;\n     object-position: center;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 60pt;\n}\n\n.active-image.active {\n  border-radius: 0;\n}\n\n.second {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  overflow: hidden;\n}\n\n@media all and (max-width: 800px) {\n  .column-half {\n    flex-basis: 100%;\n    max-width: 100%;\n    padding: 20px 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXERvbWluaWtcXERvY3VtZW50c1xcYWJpLXNlcndrb20vc3JjXFxhcHBcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLDhCQUFBO0VBQUEsNkJBQUE7VUFBQSxtQkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FDQ0o7O0FESUk7Ozs7RUFFSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0NSOztBREFROzs7Ozs7O0VBRUksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7QUNPWjs7QURMUTs7OztFQUNJLGVBQUE7QUNVWjs7QURMQTtFQUNJLG1CQUFBO0FDUUo7O0FETEE7RUFDSSxpQ0FBQTtVQUFBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FDUUo7O0FETEE7RUFDSSxrQkFBQTtBQ1FKOztBREpJO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ09SOztBREhBO0VBQ0ksMEJBQUE7S0FBQSx1QkFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxtQkFBQTtBQ01KOztBRExJO0VBQ0ksZ0JBQUE7QUNPUjs7QURIQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNNSjs7QURIQTtFQUNJO0lBQ0ksZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsZUFBQTtFQ01OO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucm93IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdyB3cmFwO1xyXG59XHJcblxyXG4uY29sdW1uLWhhbGYge1xyXG4gICAgZmxleC1iYXNpczogNTAlO1xyXG4gICAgbWF4LXdpZHRoOiA1MCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmxpbmtzLFxyXG4ucGFnaW5hdGlvbiB7XHJcbiAgICBzcGFuLFxyXG4gICAgbGkge1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgICAgICYuYWN0aXZlLFxyXG4gICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICAgICAgICBjb2xvcjogIzE3NUJDRTtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAjMTc1QkNFO1xyXG4gICAgICAgIH1cclxuICAgICAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5saW5rcyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEuMmVtO1xyXG59XHJcblxyXG4udGV4dC1yb3RhdGlvbiB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5sb2FkaW5nLWluZGljYXRvciB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbi5wYWdpbmF0aW9uIHtcclxuICAgIHNwYW4ge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBtYXJnaW46IDAgNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDlweCAxNHB4O1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzM7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hY3RpdmUtaW1hZ2Uge1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDYwcHQ7XHJcbiAgICAmLmFjdGl2ZSB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgIH1cclxufVxyXG5cclxuLnNlY29uZCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcclxuICAgIC5jb2x1bW4taGFsZiB7XHJcbiAgICAgICAgZmxleC1iYXNpczogMTAwJTtcclxuICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgfVxyXG59IiwiLnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93IHdyYXA7XG59XG5cbi5jb2x1bW4taGFsZiB7XG4gIGZsZXgtYmFzaXM6IDUwJTtcbiAgbWF4LXdpZHRoOiA1MCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubGlua3Mgc3Bhbixcbi5saW5rcyBsaSxcbi5wYWdpbmF0aW9uIHNwYW4sXG4ucGFnaW5hdGlvbiBsaSB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG59XG4ubGlua3Mgc3Bhbi5hY3RpdmUsIC5saW5rcyBzcGFuOmhvdmVyLFxuLmxpbmtzIGxpLmFjdGl2ZSxcbi5saW5rcyBsaTpob3Zlcixcbi5wYWdpbmF0aW9uIHNwYW4uYWN0aXZlLFxuLnBhZ2luYXRpb24gc3Bhbjpob3Zlcixcbi5wYWdpbmF0aW9uIGxpLmFjdGl2ZSxcbi5wYWdpbmF0aW9uIGxpOmhvdmVyIHtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgY29sb3I6ICMxNzVCQ0U7XG4gIGJvcmRlci1jb2xvcjogIzE3NUJDRTtcbn1cbi5saW5rcyBzcGFuLmFjdGl2ZSxcbi5saW5rcyBsaS5hY3RpdmUsXG4ucGFnaW5hdGlvbiBzcGFuLmFjdGl2ZSxcbi5wYWdpbmF0aW9uIGxpLmFjdGl2ZSB7XG4gIGN1cnNvcjogZGVmYXVsdDtcbn1cblxuLmxpbmtzIHtcbiAgcGFkZGluZy1sZWZ0OiAxLjJlbTtcbn1cblxuLnRleHQtcm90YXRpb24ge1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubG9hZGluZy1pbmRpY2F0b3Ige1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5wYWdpbmF0aW9uIHNwYW4ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogMCA1cHg7XG4gIHBhZGRpbmc6IDlweCAxNHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjMzMzO1xufVxuXG4uYWN0aXZlLWltYWdlIHtcbiAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xuICBib3JkZXItcmFkaXVzOiA2MHB0O1xufVxuLmFjdGl2ZS1pbWFnZS5hY3RpdmUge1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG4uc2Vjb25kIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XG4gIC5jb2x1bW4taGFsZiB7XG4gICAgZmxleC1iYXNpczogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjBweCAwO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _image_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../image.service */ "./src/app/image.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let HomeComponent = class HomeComponent {
    constructor(imageService) {
        this.imageService = imageService;
        this.pageIndex = 1;
        this.pageSize = 12;
        this.isLoading = true;
        this._isRefreshAvailable = true;
        this.refreshSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.subscription = this.refreshSubject.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(300)).subscribe(x => {
            this._isRefreshAvailable = true;
        });
    }
    getImages() {
        this.isLoading = true;
        this.imageService.getImages(this.pageIndex, this.pageSize).subscribe((res) => {
            const _l = res.headers.get('link');
            const link = this.imageService.parseLinkHeader(_l);
            if (link) {
                this.pagination = link;
            }
            if (res) {
                this.imageData = res.body;
            }
        }, err => { }, () => {
            this.isLoading = false;
        });
    }
    onPage(params) {
        if (this._isRefreshAvailable) {
            this._isRefreshAvailable = false;
            this.pageIndex = params._page;
            this.pageSize = params._limit;
            this.getImages();
        }
        this.refreshSubject.next();
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    ngOnInit() {
        this.getImages();
    }
};
HomeComponent.ctorParameters = () => [
    { type: _image_service__WEBPACK_IMPORTED_MODULE_2__["ImageService"] }
];
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
        styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/image-hover.directive.ts":
/*!******************************************!*\
  !*** ./src/app/image-hover.directive.ts ***!
  \******************************************/
/*! exports provided: ImageHoverDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageHoverDirective", function() { return ImageHoverDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ImageHoverDirective = class ImageHoverDirective {
    constructor(elementRef, renderer) {
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    onMouseEnter() {
        if (this.appImageHover) {
            this.renderer.setAttribute(this.elementRef.nativeElement, 'src', this.appImageHover.url);
            // this.renderer.addClass(this.elementRef.nativeElement, 'active');
        }
    }
    onMouseLeave() {
        if (this.appImageHover) {
            this.renderer.setAttribute(this.elementRef.nativeElement, 'src', this.appImageHover.thumbnailUrl);
            // this.renderer.removeClass(this.elementRef.nativeElement, 'active');
        }
    }
};
ImageHoverDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ImageHoverDirective.prototype, "appImageHover", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseenter')
], ImageHoverDirective.prototype, "onMouseEnter", null);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('mouseleave')
], ImageHoverDirective.prototype, "onMouseLeave", null);
ImageHoverDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appImageHover]'
    })
], ImageHoverDirective);



/***/ }),

/***/ "./src/app/image.service.ts":
/*!**********************************!*\
  !*** ./src/app/image.service.ts ***!
  \**********************************/
/*! exports provided: baseUrl, ImageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageService", function() { return ImageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



const baseUrl = 'https://jsonplaceholder.typicode.com';
let ImageService = class ImageService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.url = '/photos';
        this.url = baseUrl + this.url;
    }
    getImages(pageIndex, pageSize = 12) {
        let query = '?';
        if (pageIndex) {
            query += '_page=' + pageIndex;
        }
        if (pageSize) {
            query += '&_limit=' + pageSize;
        }
        return this.httpClient.get(this.url + query, { observe: 'response' });
    }
    parseLinkHeader(header) {
        if (header.length === 0) {
            throw new Error('input must not be of zero length');
        }
        const result = [];
        // Split parts by comma and parse each part into a named link
        return header.split(/(?!\B"[^"]*),(?![^"]*"\B)/).reduce((links, part) => {
            const section = part.split(/(?!\B"[^"]*);(?![^"]*"\B)/);
            if (section.length < 2) {
                throw new Error('section could not be split on \';\'');
            }
            const url = section[0].replace(/<(.*)>/, '$1').trim();
            const q = {};
            url.split('?')[1].split('&').forEach(i => {
                q[i.split('=')[0]] = i.split('=')[1];
            });
            const name = section[1].replace(/rel="(.*)"/, '$1').trim();
            result.push({ url, params: q, name });
            return result;
        }, {});
    }
};
ImageService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ImageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ImageService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Dominik\Documents\abi-serwkom\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map